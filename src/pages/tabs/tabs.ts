import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { BookingsPage } from '../bookings/bookings';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-tabs',
  template: `
    <ion-tabs>
      <ion-tab tabIcon="home" tabTitle="Home" [root]="tab1"></ion-tab>
      <ion-tab tabIcon="list-box" tabTitle="My Bookings" [root]="tab2"></ion-tab>
      <ion-tab tabIcon="contact" tabTitle="Profile" [root]="tab3"></ion-tab>
    </ion-tabs>`
})
export class TabsPage {
  tab1: any;
  tab2: any;
  tab3:any;
  constructor() {
    this.tab1 = HomePage;
    this.tab2 = BookingsPage;
    this.tab3 = ProfilePage;
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
