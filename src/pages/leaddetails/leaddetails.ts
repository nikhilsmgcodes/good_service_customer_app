import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, ModalController } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map'; 

import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import { ToastProvider } from '../../providers/toast/toast';
/**
 * Generated class for the LeaddetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaddetails',
  templateUrl: 'leaddetails.html',
})
export class LeaddetailsPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;

  withoutGstBills:any;
  withGstBills:any;

  leadinfo:any;
  leadsDetails : any ={category:null, brand:null,description:null,date:null,status:null};
  customerAddress : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  followupmsg : any;
  lead:any = 'details';
  withGstBills_length:boolean=false;
  withoutGstBills_length:boolean=false;
  
  constructor(public navCtrl: NavController,public alertCtrl: AlertController,
    public modalCtrl : ModalController, public navParams: NavParams, public http:Http, public toast: ToastProvider) {
    this.leadinfo = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaddetailsPage');
    this.getLeadInfo(this.leadinfo.l_id);
    // this.getFollowUpInfo(this.leadinfo.l_id);
    this.allBills();
  }

  public viewBill(b){
    var modalPage = this.modalCtrl.create('BilldetailsPage', { bill: b});
    console.log(b);
    modalPage.present();
  }

  public allBills(){
    this.data = 'l_id='+this.leadinfo.l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'bills',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.withoutGstBills=data.withoutGstBills;
          this.withGstBills=data.withGstBills;
          this.withoutGstBills_length = (data.withoutGstBills.length>0)?true:false;
          this.withGstBills_length = (data.withGstBills.length>0)?true:false;
        }
      }
    });
  }

  public reOpen(completed_date,l_id){
    let alert=this.alertCtrl.create({
      message:'Reopen Lead?',
      buttons:[{
           text:'Cancel',
           handler:()=>{
             console.log('cancel clicked');
           }
      },
        {
            text:'Ok',
            handler:()=>{
              this.data = "l_id="+l_id+"&completed_date="+completed_date;
                this.httpOptions = {
                headers: new Headers({
                  'Content-Type':  'application/x-www-form-urlencoded',
                  'user-phone': localStorage.getItem('user_phone'),
                  'session-key': JSON.parse(localStorage.getItem('session_key')),
                  'role': localStorage.getItem('role')
                })
              };
            this.http.post('http://'+this.url+'reopenLead', this.data, this.httpOptions).map(res => res.json()).subscribe(
              data => {
                if(data.sessionExpire){
                  this.navCtrl.setRoot(LoginPage);
                  this.toast.notify(data.message,'toast-error');
                  }
                else{
                  if(data.error){
                    this.toast.notify(data.message,'toast-error');
                  }
                  else{
                    this.toast.notify(data.message,'toast-error');
                    this.navCtrl.setRoot(TabsPage);
                  };
                }

  });
}
            }
      ]
    });
      alert.present();
  }

  public getLeadInfo(l_id){
    this.data = 'l_id='+l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': localStorage.getItem('user_phone'),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getLeadDetailByIdAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.leadsDetails = data.lead_data[0];
          this.customerAddress = data.customer_address[0];
        }
      }
    });
  }

  public getFollowUpInfo(l_id){
    this.data = 'l_id='+l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': localStorage.getItem('user_phone'),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getFollowUpInfoAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.followupmsg=data.msg;
        }
      }
    });
  }

}
