import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { ToastProvider } from '../../providers/toast/toast';

import {LoginPage} from '../login/login';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;

  customerInfo:any;
  customer_name:any;
  customer_email:any;
  constructor(public toast: ToastProvider, public nav: Nav, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public http:Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getCustomerInfo();
  }

  editProfile(){
    document.getElementById('profilePage').style.display='none';
    document.getElementById('editPage').style.display='block';
    document.getElementById('editButton').style.display='none';
    document.getElementById('updateButton').style.display='block';
    document.getElementById('cancelButton').style.display='block';
    this.customer_name = this.customerInfo.name;
    this.customer_email = this.customerInfo.email;
  }

  cancel(){
    document.getElementById('updateButton').style.display='none';
    document.getElementById('cancelButton').style.display='none';
    document.getElementById('editButton').style.display='block';
    document.getElementById('editPage').style.display='none';
    document.getElementById('profilePage').style.display='block';
  }

  updateProfile(){
    if(this.customer_name == this.customerInfo.name && this.customer_email == this.customerInfo.email){
      this.toast.notify('No Changes To Update','toast-error');
    }
    else if(this.customer_name == '' || this.customer_email === ''){
      this.toast.notify('Please fill the fields','toast-error');
    } 
    else {
      this.data = 'c_name='+this.customer_name+'&c_email='+this.customer_email+'&c_phone='+this.customerInfo.c_phone;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'updateCustomerProfile',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.toast.notify(data.message,'toast-success');
          document.getElementById('updateButton').style.display='none';
          document.getElementById('cancelButton').style.display='none';
          document.getElementById('editButton').style.display='block';
          document.getElementById('editPage').style.display='none';
          document.getElementById('profilePage').style.display='block';
          this.getCustomerInfo();
        }
      }
    });
    }
    
  }

  logout(){
    let alert=this.alertCtrl.create({
      message:'Are you sure you want to log out?',
      buttons:[{
           text:'Cancel',
           handler:()=>{
             console.log('cancel clicked');
           }
      },
        {
            text:'Ok',
            handler:()=>{
              localStorage.clear();
              this.nav.setRoot(LoginPage);
            }
      }
      ]
    });
      alert.present();
  }

  public getCustomerInfo(){
    //this.data = 'l_id='+l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': localStorage.getItem('user_phone'),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'getCustomerInfo', this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.customerInfo = data.data[0];
        }
      }
    });
  }
}
