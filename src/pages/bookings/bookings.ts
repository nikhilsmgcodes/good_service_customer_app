import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { ToastProvider } from '../../providers/toast/toast';

import { LoginPage } from '../login/login';
import { LeaddetailsPage } from '../leaddetails/leaddetails';
/**
 * Generated class for the BookingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookings',
  templateUrl: 'bookings.html',
})
export class BookingsPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : any;

  phone:any = localStorage.getItem('user_phone');
  allLeads: any;
  status:any = 'all';

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toast: ToastProvider) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad BookingsPage')
    this.getLeads(this.status);;
  }

  public itemSelected(x){
    this.navCtrl.push(LeaddetailsPage,{data:x});
  }

  getLeads(status){
    this.allLeads=null;
    this.data = "status="+status+'&phone='+this.phone;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getLeadsCustomer',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.allLeads = null;
          this.toast.notify("There is no bookings.",'toast-error');
        }
        else{
          this.allLeads=data.data;
        }
      }
    });
  }

}
