import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { TabsPage } from '../tabs/tabs'

import { ToastProvider } from '../../providers/toast/toast';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;
  userPhone : any = null;
  userPass : any = null;
  otp: any = null;
  newPassword:any = null;
  confirmPassword: any =null;
  userName: any = null;
  userEmail: any = null;

  tabBarElement:any;
  constructor(public navCtrl: NavController,private menu: MenuController, public navParams: NavParams, public http: Http, public toast: ToastProvider) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.swipeEnable(false);
    if (this.tabBarElement != null) {
      this.tabBarElement.style.display = 'none'; // or whichever property which you want to access
    }
    this.checkSession();
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
    if (this.tabBarElement != null) {
      this.tabBarElement.style.display = 'flex';
    }
   }

  public checkSession(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': localStorage.getItem('user_phone'),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'checkSessionAdmin',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.error){
        // this.navCtrl.push(LoginPage);
      }
      else{
        this.navCtrl.setRoot(TabsPage);
        //window.location.reload();
      }
    })
  }

  public doLogin(){
    if(this.userPhone == null || this.userPhone.length != 10){
      this.toast.notify("Please Enter 10 digit Phone Number",'toast-error');
      return;
    }
    if(this.userPass == null){
      this.toast.notify("Please Enter Password",'toast-error');
      return;
    }
    else{
      this.data = "user_phone="+this.userPhone+'&user_pass='+this.userPass+'&role='+"customer";

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'adminLogin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            localStorage.setItem('session_key', JSON.stringify(data.session_key));
            localStorage.setItem('user_phone', this.userPhone);
            localStorage.setItem('role', "customer");
            //localStorage.setItem('tableName', 'admin');
            this.navCtrl.setRoot(TabsPage);
          }
        });
      }
  }

  showGetOtpForgot(){
    document.getElementById('login').style.display = "none";
    document.getElementById('showGetOtpForgot').style.display = "block";
  }

  login(){
    this.otp=null;
    this.newPassword = null;
    this.confirmPassword = null;
    this.userPhone = null;
    this.userPass = null;
    this.userName = null;
    this.userEmail = null;
    document.getElementById('showGetOtpForgot').style.display = "none";
    document.getElementById('showVerifyOtpForgot').style.display = "none";
    document.getElementById('showResetPassword').style.display = "none";
    document.getElementById('showSignUpGetOtp').style.display = "none";
    document.getElementById('showVerifyOtpSignUp').style.display = "none";
    document.getElementById('showSignUp').style.display = "none";
    document.getElementById('login').style.display = "block";
  }

  getOtpForgot(){
    if(this.userPhone == null || this.userPhone.length != 10){
      this.toast.notify("Please Enter 10 digit Phone Number",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&role='+"customer";

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'getOTP', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            document.getElementById('showGetOtpForgot').style.display = "none";
            document.getElementById('showVerifyOtpForgot').style.display = "block";
          }
        });
  }

  verifyOTP(){
    if(this.otp == null){
      this.toast.notify("Please Enter OTP",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&otp='+this.otp;

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'verifyOTP', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.otp = null;
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            this.otp = null;
            document.getElementById('showVerifyOtpForgot').style.display = "none";
            document.getElementById('showResetPassword').style.display = "block";
          }
        });
  }

  resetPassword(){
    if(this.newPassword == null){
      this.toast.notify("Please Enter new Password",'toast-error');
      return;
    }
    if(this.newPassword != this.confirmPassword){
      this.toast.notify("Password dosn't Match",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&password='+this.newPassword+'&role='+'customer';

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'resetPassword', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            this.newPassword = null;
            this.confirmPassword = null;
            this.otp = null;
            document.getElementById('showResetPassword').style.display = "none";
            document.getElementById('login').style.display = "block";
          }
        });
  }

  showSignUpGetOtp(){
    this.userPass = null;
    document.getElementById('login').style.display = "none";
    document.getElementById('showSignUpGetOtp').style.display = "block";
  }

  getOtpSignUp(){
    if(this.userPhone == null || this.userPhone.length != 10){
      this.toast.notify("Please Enter 10 digit Phone Number",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&role='+"customer"+'&status=registration';

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'getOTP', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            document.getElementById('showSignUpGetOtp').style.display = "none";
            document.getElementById('showVerifyOtpSignUp').style.display = "block";
          }
        });
  }

  verifySignUpOTP(){
    if(this.otp == null){
      this.toast.notify("Please Enter OTP",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&otp='+this.otp;

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'verifyOTP', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.otp = null;
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            this.otp = null;
            document.getElementById('showVerifyOtpSignUp').style.display = "none";
            document.getElementById('showSignUp').style.display = "block";
          }
        });
  }

  signUp(){
    if(this.userName == null || this.userEmail == null || this.userPass == null){
      this.toast.notify("Please Fill all the fields",'toast-error');
      return;
    }
    this.data = "user_phone="+this.userPhone+'&user_name='+this.userName+'&user_email='+this.userEmail+'&user_password='+this.userPass;

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'customerSignUp', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            this.toast.notify(data.message,'toast-success');
            this.userName = null;
            this.userEmail = null;
            this.userPass = null;
            this.otp = null;
            document.getElementById('showSignUp').style.display = "none";
            document.getElementById('login').style.display = "block";          }
        });
  }
}
