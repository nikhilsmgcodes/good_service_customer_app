import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AddleadPage } from '../addlead/addlead';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  category:any;
  constructor(public navCtrl: NavController) {

  }

  public getCategory(category){
    this.category = category;
    this.navCtrl.push(AddleadPage,{data:this.category});
  }

}
