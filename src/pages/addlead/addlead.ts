import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { ToastProvider } from '../../providers/toast/toast';

import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
/**
 * Generated class for the AddleadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addlead',
  templateUrl: 'addlead.html',
})
export class AddleadPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : any;

  category:any;
  brand: any = 'LG';
  description:any;
  phone:any = localStorage.getItem('user_phone');
  categoryList: string[] = ['LG','SONY','SAMSUNG','IFB','WHIRLPOOL','BOSCH','SIEMENS','VIDEOCON','ONIDA','OTHERS'];
  customerAddress : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  c_name: any;
  c_email:any;
  c_address: any;
  address: any = '';
  city: any = '';
  state: any = '';
  zip: any = '';
  optional_phone: any = '';
  ca_id:any;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toast: ToastProvider) {
    this.category = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddleadPage');
    this.getCustomerAddress();
  }

  openSecond(){
    this.address = null;
    this.city = null;
    this.state = null;
    this.zip = null;
    this.optional_phone = null;
    document.getElementById('first').style.display='none';
    document.getElementById('third').style.display='none';
    document.getElementById('second').style.display='block';
  }

  showFirst(){
    document.getElementById('second').style.display='none';
    document.getElementById('third').style.display='none';
    document.getElementById('first').style.display='block';
  }

  showThird(){
    document.getElementById('second').style.display='none';
    document.getElementById('first').style.display='none';
    document.getElementById('third').style.display='block';
  }
  
  addLead(){
    if(this.ca_id == null){
      this.toast.notify('Please select the Address','toast-error');
      return;
    }
    
    this.data = 'c_phone='+this.phone+'&ca_id='+this.ca_id+'&category='+this.category+'&brand='+this.brand+'&description='+this.description;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'addNewLeadAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.toast.notify("Booked Successfully. Team will Contact you shortly.",'toast-success');
          this.navCtrl.setRoot(TabsPage);
        }
      }
    });
  }

  addNewAddress(){
    if(this.address=='null' || this.city=='null' || this.state=='null' || this.zip=='null')
    {
      this.toast.notify('Please Fill All Details','toast-error');
      return;
    }
    this.data = 'c_phone='+this.phone+'&name='+this.c_name+'&email='+this.c_email+'&address='+this.address+'&city='+this.city+'&state='+this.state+'&zip='+this.zip+'&optional_phone='+this.optional_phone;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'addNewCustomerAddressAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.toast.notify("New Address Added.",'toast-success');
          this.address = null;
          this.city = null;
          this.state = null;
          this.zip = null;
          this.optional_phone = null;
          this.c_name = null;
          this.c_email = null;
          this.openSecond();
          this.getCustomerAddress();
        }
      }
    });
  }

  getCustomerAddress(){
    this.data = 'c_phone='+this.phone;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getCustomerAddressByPhoneAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.c_address=data.data;
          this.c_name = data.data[0].name;
          this.c_email = data.data[0].email;
        }
      }
    });
  }

}
