import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddleadPage } from './addlead';

@NgModule({
  declarations: [
    AddleadPage,
  ],
  imports: [
    IonicPageModule.forChild(AddleadPage),
  ],
})
export class AddleadPageModule {}
