import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { InfoPage } from '../pages/info/info';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, iconimg: string, component: any}>;

  constructor(public platform: Platform, public alertCtrl:AlertController, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'HOME', iconimg:'home', component: TabsPage },  
      { title: 'ABOUT', iconimg:'information-circle', component: null },
      //{ title: 'T&C', iconimg:'information-circle', component: null },
      //{ title: 'SHARE', iconimg:'share', component: TabsPage },
      { title: 'CONTACT US', iconimg:'call', component: null },
      { title: 'LOGOUT', iconimg:'exit', component: null}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if(page.title == 'ABOUT'){
      this.nav.push(InfoPage, {data:'ABOUT'});
    }
    if(page.title == 'CONTACT US'){
      this.nav.push(InfoPage, {data:'CONTACT US'});
    }

    if(page.title=='LOGOUT'){
      let alert=this.alertCtrl.create({
        message:'Are you sure you want to log out?',
        buttons:[{
             text:'Cancel',
             handler:()=>{
               console.log('cancel clicked');
             }
        },
          {
              text:'Ok',
              handler:()=>{
                localStorage.clear();
                this.nav.setRoot(LoginPage);
              }
        }
        ]
      });
        alert.present();
    }
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
